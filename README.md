# Toodledo Neodark Theme

Dark CSS theme for parts of [Toodledo website](http://toodledo.com/). Namely only its "Tasks" part, including the most of its premium features.

[The official theme website](https://userstyles.org/styles/164894/toodledo-neodark)

![screenshot](screenshot.jpg)

Developed and tested only on Firefox 62 - 65. Should work in Chrome too.

Please consider up-voting [feature request](https://roadmap.toodledo.com/feature-requests/p/dark-theme) for official support of a Toodledo dark theme.

## Installation

1. Have Firefox or Chrome.
2. Have [Stylus add-on](https://addons.mozilla.org/en-US/firefox/addon/styl-us/) ([GitHub](https://github.com/openstyles/stylus)) or its official closed-source alternative - [Stylish](https://addons.mozilla.org/en-US/firefox/addon/stylish/).
3. Apply this theme to the add-on: **a)** through the [official website](https://userstyles.org/styles/164894/toodledo-neodark) **or b)** insert the included CSS directly into the add-on.
4. Refresh the Toodledo website.